#!/bin/bash

# Usage ./install_jdk_ubuntu.sh version
# version - number of jdk version

sudo apt-get remove openjdk* 
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update



sudo apt-get install oracle-java$1-installer
